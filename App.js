import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';

import {
  Colors,
} from 'react-native/Libraries/NewAppScreen';
import Routing from './src/simpleNetworking/Routing';


const App = () => {

  return (
    <SafeAreaView style={{flex: 1}}>
        <Routing />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
});

export default App;
